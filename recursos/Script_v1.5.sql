-- MariaDB dump 10.19  Distrib 10.4.24-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: db_inventario
-- ------------------------------------------------------
-- Server version	10.4.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_categoria`
--

Create database if not exists db_inventario;

use db_inventario;

DROP TABLE IF EXISTS `tb_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_categoria` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nom_categoria` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `estado_categoria` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_categoria`),
  UNIQUE KEY `UQ_nomCategoria` (`nom_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=1029 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_categoria`
--

LOCK TABLES `tb_categoria` WRITE;
/*!40000 ALTER TABLE `tb_categoria` DISABLE KEYS */;
INSERT INTO `tb_categoria` VALUES (1000,'celular',1),(1008,' Snacks',1),(1027,'celulares',1);
/*!40000 ALTER TABLE `tb_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_producto`
--

DROP TABLE IF EXISTS `tb_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_producto` (
  `id_producto` int(11) NOT NULL,
  `nom_producto` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `des_producto` varchar(90) CHARACTER SET utf8mb4 DEFAULT NULL,
  `stock` double(6,2) DEFAULT NULL,
  `precio` decimal(6,2) DEFAULT NULL,
  `unidad_de_medida` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `estado_producto` tinyint(1) DEFAULT NULL,
  `categoria` int(5) DEFAULT NULL,
  `fecha_entrada` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_producto`),
  UNIQUE KEY `UQ_nomProducto` (`nom_producto`),
  KEY `FK_producto_categoria` (`categoria`),
  CONSTRAINT `FK_producto_categoria` FOREIGN KEY (`categoria`) REFERENCES `tb_categoria` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_producto`
--

LOCK TABLES `tb_producto` WRITE;
/*!40000 ALTER TABLE `tb_producto` DISABLE KEYS */;
INSERT INTO `tb_producto` VALUES (1292,'Pringles','Papas onduladas',9.99,1.25,'Empaque',1,1008,'2022-06-05 06:00:00'),(1392,'Pringles de especias y cebolla','Papas onduladas sabor a especias y cebollas',9.99,1.25,'Empaque',1,1008,'2022-06-05 06:00:00'),(2031,'Galaxy a10s','2GB RAM. 32GB ROM',9.99,9.99,'Dispositivo',1,1000,'2022-06-26 06:00:00'),(2398,'Cheetos','Snacks de tipo palito sabor a queso ',3.00,0.25,'Empaque',1,1008,'2022-06-05 06:00:00'),(4793,'Zibas de especias y crema','Snack sabor a especiaas y crema',2.00,7.00,'Empaque',1,1008,'2022-06-05 06:00:00');
/*!40000 ALTER TABLE `tb_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_usuario`
--

DROP TABLE IF EXISTS `tb_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario` (
  `nombre` varchar(45) CHARACTER SET utf8mb4 DEFAULT NULL,
  `correo` varchar(125) CHARACTER SET utf8mb4 DEFAULT NULL,
  `usuario` varchar(40) CHARACTER SET utf8mb4 DEFAULT NULL,
  `clave` varchar(150) CHARACTER SET utf8mb4 DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `tipo` tinyint(1) DEFAULT NULL,
  `pregunta` varchar(65) CHARACTER SET utf8mb4 DEFAULT NULL,
  `respuesta` varchar(40) CHARACTER SET utf8mb4 DEFAULT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT curdate(),
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apellido` varchar(45) CHARACTER SET utf8mb4 DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `correo` (`correo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_usuario`
--

LOCK TABLES `tb_usuario` WRITE;
/*!40000 ALTER TABLE `tb_usuario` DISABLE KEYS */;
INSERT INTO `tb_usuario` VALUES ('Francisco','fran@gmail.com','fran','1234',1,1,'Segundo nombre?','Jonatan','2022-06-16 06:00:00',1,'Abarca',NULL),('Nestor Ãaron ','nestor@gmail.com','nestroclash','itca',1,1,'fecha de nacimiento?','12-12-2003','2022-06-18 06:00:00',4,'Orellana Lopez',NULL),('Neftali','neftali@gmail.com','neftali','123456',1,1,'Mi primer nombre?','Neftali','2022-06-19 06:00:00',5,'CalderÃ³n',78902134),('Ãaron ','aaron@gmail.com','aaron','itca123',1,1,'Mi primer nombre?','aaron','2022-06-23 06:00:00',6,'LopÃ©z',90098989);
/*!40000 ALTER TABLE `tb_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-27 18:12:17
