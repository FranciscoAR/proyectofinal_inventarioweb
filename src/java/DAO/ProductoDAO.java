package DAO;

import java.util.List;
import model.Producto;


public interface ProductoDAO {
   
    public List<Producto> listar();
    public boolean registrarProducto (Producto prod);
    public boolean actualizarProducto (Producto prod);
    public boolean borrarProducto(int id_prod);
    public Producto editarProducto(int id_prod);
    
}
