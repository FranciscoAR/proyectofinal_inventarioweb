package DAO;

import java.util.*;
import model.Categoria;
import factory.ConexionDB;
import factory.FactoryConexionDB;
import java.sql.*;
import model.Producto;


public class CategoriaDAOImplementar implements CategoriaDAO {

    ConexionDB conn; //Crear el objeto tipo conexion
    
    public CategoriaDAOImplementar()
    {
        //Definir la base de datos que se conectará por defecto
        
    }
    
    @Override
    public List<Categoria> Listar() {
        this.conn = FactoryConexionDB.open(FactoryConexionDB.MYSQL);
        StringBuilder miSql = new StringBuilder(); //Construir la consulta
        miSql.append("Select * From tb_categoria"); //Agregar la consulta
        List<Categoria> lista = new ArrayList<Categoria>();
        try
        {
            //Se crea el objeto ResultSet implementando el método (consultaSQL) creado para la consulta
            ResultSet rs = this.conn.consultaSQl(miSql.toString());
            while(rs.next())
            {
                Categoria categoria = new Categoria(); //Declarar el objeto categoria 
                //Asignar cada campo consultado al atributo en la clase
                categoria.setId_categoria(rs.getInt("id_categoria"));
                categoria.setNom_categoria(rs.getString("nom_categoria"));
                categoria.setEstado_categoria(rs.getInt("estado_categoria"));
                lista.add(categoria); //Agregar al array cada registro encontrado
            }
            
        }catch(SQLException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            this.conn.cerrarConexion();
        }
        return lista;
    }

    @Override
    public List<Categoria> Listar2() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Categoria editarCat(int id_cat_edit) {
        this.conn = FactoryConexionDB.open(FactoryConexionDB.MYSQL); //Hacer la conexion
        Categoria categoria = new Categoria(); //Objeto categoria para devolver datos
        StringBuilder miSql = new StringBuilder();//Construir la consulta
        //Agregar la consulta SQL
        miSql.append("Select * From tb_categoria where id_categoria = ").append(id_cat_edit);
        try //Realizar la consulta
        {
            ResultSet rs = this.conn.consultaSQl(miSql.toString());
            while(rs.next())
            {
                categoria.setId_categoria(rs.getInt("id_categoria"));
                categoria.setNom_categoria(rs.getString("nom_categoria"));
                categoria.setEstado_categoria(rs.getInt("estado_categoria"));
            }
                
        }catch(SQLException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            this.conn.cerrarConexion();
        }
        
        return categoria;
    }

    @Override
    public boolean guardarCat(Categoria categoria) {
        this.conn = FactoryConexionDB.open(FactoryConexionDB.MYSQL); //Hacer la conexion
        boolean guarda = false; //Bandera de resultado
        try
        {
            if(categoria.getId_categoria() == 0) //Para cuando es una nueva categoria
            {
                StringBuilder miSql = new StringBuilder();
                miSql.append("insert into tb_categoria (nom_categoria, estado_categoria ) values ('");
                miSql.append(categoria.getNom_categoria()).append("', ").append(categoria.getEstado_categoria());
                miSql.append(");");
                this.conn.ejecutarSQl(miSql.toString());
            }else if (categoria.getId_categoria() > 0)
            {
                StringBuilder miSql = new StringBuilder();
                miSql.append("Update tb_categoria set nom_categoria = '");
                miSql.append(categoria.getNom_categoria());
                miSql.append("', estado_categoria = ");
                miSql.append(categoria.getEstado_categoria());
                miSql.append(" where id_categoria = ").append(categoria.getId_categoria());
                this.conn.ejecutarSQl(miSql.toString());
            }
            guarda = true;
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            this.conn.cerrarConexion();
        }
        return guarda;
    }

    @Override
    public boolean borrarCat(int id_cat_borrar) {
        this.conn = FactoryConexionDB.open(FactoryConexionDB.MYSQL); //Hacer la conexion
        boolean borrar = false; //Bandera de resultado
        try
        {
            StringBuilder miSql = new StringBuilder();
            miSql.append("Delete from tb_categoria where id_categoria = ");
            miSql.append(id_cat_borrar);
            this.conn.ejecutarSQl(miSql.toString());
            borrar = true;
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            this.conn.cerrarConexion();
        }
        return borrar;
    }

    @Override
    public List<Producto> productoElimar(int cat_eliminar) {
        this.conn = FactoryConexionDB.open(FactoryConexionDB.MYSQL); //Hacer la conexion
        List<Producto> prodEliminar = new ArrayList<Producto>();
        StringBuilder miSQL = new StringBuilder();
        try
        {
            miSQL.append("Select prod.nom_producto, prod.stock, prod.estado_producto from tb_producto as prod where prod.categoria = ");
            miSQL.append(cat_eliminar);
            miSQL.append(";");
            
            ResultSet rs = null;
            
            rs = this.conn.consultaSQl(miSQL.toString());
            while(rs.next())
            {
                
                Producto prod = new Producto();
                prod.setNom_producto(rs.getString("nom_producto"));
                prod.setStock(rs.getFloat("stock"));
                prod.setEstado(rs.getInt("estado_producto"));
                prodEliminar.add(prod);
            }
            
            
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            this.conn.cerrarConexion();
        }
        
        return prodEliminar;
    }
    
    
}
