package DAO;


import java.util.ArrayList;
import model.Usuario;

public interface UsuarioDAO {
    
    public ArrayList<Usuario> startSesion(String user, String clave);
    public boolean RegistrarUsuario(Usuario usuario);
    public String recuperarCorreo(long telefono);
    public String recuperarContraseña (String correo);
    public String pregunta(String correo);
    public boolean modificarContraseña(String correo, String contraseñaActual, String nuevaContraseña);
    public String respuesta(String correo, String pregunta);
    public String verificarCorreo(String correo);
    
}
