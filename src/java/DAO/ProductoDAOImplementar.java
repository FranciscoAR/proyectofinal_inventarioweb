package DAO;

import factory.ConexionDB;
import factory.FactoryConexionDB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Producto;

public class ProductoDAOImplementar implements ProductoDAO{

    ConexionDB conn;

    public ProductoDAOImplementar() {
        
    }
    
    @Override
    public List<Producto> listar() {
        this.conn = FactoryConexionDB.open(FactoryConexionDB.MYSQL);
        StringBuilder miSQL = new StringBuilder();
        List<Producto> lista = new ArrayList<Producto>();
        try
        {
            miSQL.append("Select prod.id_producto, prod.nom_producto, prod.des_producto, prod.stock, prod.precio, prod.unidad_de_medida, prod.estado_producto, cat.nom_categoria, prod.fecha_entrada from tb_producto as prod inner join tb_categoria as cat on prod.categoria = cat.id_categoria");
            ResultSet rs = null;
            
            rs = this.conn.consultaSQl(miSQL.toString());
            
            while(rs.next())
            {
                Producto prod = new Producto();
                prod.setId_producto(rs.getInt("id_producto"));
                prod.setNom_producto(rs.getString("nom_producto"));
                prod.setDesc_producto(rs.getString("des_producto"));
                prod.setStock(rs.getFloat("stock"));
                prod.setPrecio(rs.getFloat("precio"));
                prod.setUnidadMedida(rs.getString("unidad_de_medida"));
                prod.setEstado(rs.getInt("estado_producto"));
                prod.setCategoria(rs.getString("nom_categoria"));
                prod.setFecha_entrada(rs.getDate("fecha_entrada"));
                
                lista.add(prod);
            }
        }
        catch(SQLException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            this.conn.cerrarConexion();
        }
        
        return lista;
    }

    @Override
    public boolean registrarProducto(Producto prod) {
          this.conn = FactoryConexionDB.open(FactoryConexionDB.MYSQL);
          StringBuilder miSQL = new StringBuilder();
          Boolean registrar = false;
          try
          {
              miSQL.append(" insert into tb_producto(id_producto, nom_producto, des_producto, stock, precio, ");
              miSQL.append("unidad_de_medida, estado_producto, categoria)  values(");
              miSQL.append(prod.getId_producto());
              miSQL.append(", '");
              miSQL.append(prod.getNom_producto());
              miSQL.append("', '");
              miSQL.append(prod.getDesc_producto());
              miSQL.append("', ");
              miSQL.append(prod.getStock());
              miSQL.append(", ");
              miSQL.append(prod.getPrecio());
              miSQL.append(", '");
              miSQL.append(prod.getUnidadMedida());
              miSQL.append("', ");
              miSQL.append(prod.getEstado());
              miSQL.append(", ");
              miSQL.append(prod.getCategoria());
              miSQL.append(");");
              
              System.out.println(miSQL.toString());
              if(this.conn.ejecutarSQl(miSQL.toString()))
              {
                  registrar = true;
                  return registrar;
              }
              
          }
          catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            this.conn.cerrarConexion();
        }
          return registrar;
          
    }

    @Override
    public boolean borrarProducto(int id_prod) {
        this.conn = FactoryConexionDB.open(FactoryConexionDB.MYSQL); //Hacer la conexion
        boolean borrar = false; //Bandera de resultado
        try
        {
            StringBuilder miSql = new StringBuilder();
            miSql.append("Delete from tb_producto where id_producto = ");
            miSql.append(id_prod);
            this.conn.ejecutarSQl(miSql.toString());
            borrar = true;
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            this.conn.cerrarConexion();
        }
        return borrar;
    }

    @Override
    public Producto editarProducto(int id_prod) {
        this.conn = FactoryConexionDB.open(FactoryConexionDB.MYSQL); //Hacer la conexion
        Producto prod = new Producto(); //Objeto categoria para devolver datos
        StringBuilder miSql = new StringBuilder();//Construir la consulta
        //Agregar la consulta SQL
        miSql.append("Select prod.id_producto, prod.nom_producto, prod.des_producto, prod.stock, prod.precio, prod.unidad_de_medida, prod.estado_producto, cat.nom_categoria, prod.categoria from tb_producto as prod inner join tb_categoria as cat on prod.categoria = cat.id_categoria where id_producto = ").append(id_prod);
        try //Realizar la consulta
        {
            ResultSet rs = this.conn.consultaSQl(miSql.toString());
            while(rs.next())
            {
                prod.setId_producto(rs.getInt("id_producto"));
                prod.setNom_producto(rs.getString("nom_producto"));
                prod.setDesc_producto(rs.getString("des_producto"));
                prod.setEstado(rs.getInt("estado_producto"));
                prod.setStock(rs.getFloat("stock"));
                prod.setPrecio(rs.getFloat("precio"));
                prod.setUnidadMedida(rs.getString("unidad_de_medida"));
                prod.setCategoria(rs.getString("nom_categoria"));
                prod.setId_categoria(rs.getInt("categoria"));
            }
                
        }catch(SQLException ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            this.conn.cerrarConexion();
        }
        
        return prod;
    }

    @Override
    public boolean actualizarProducto(Producto prod) {
     this.conn = FactoryConexionDB.open(FactoryConexionDB.MYSQL); //Hacer la conexion
        boolean editar = false; //Bandera de resultado
        try
        {
            StringBuilder miSQL = new StringBuilder();
            miSQL.append("Update tb_producto set nom_producto = '");
            miSQL.append(prod.getNom_producto());
            miSQL.append("',  des_producto = '");
            miSQL.append(prod.getDesc_producto());
            miSQL.append("', stock = ");
            miSQL.append(prod.getStock());
            miSQL.append(", precio = ");
            miSQL.append(prod.getPrecio());
            miSQL.append(", unidad_de_medida = '");
            miSQL.append(prod.getUnidadMedida());
            miSQL.append("', estado_producto = ");
            miSQL.append(prod.getEstado());
            miSQL.append(", categoria = ");
            miSQL.append(prod.getCategoria());
            miSQL.append(" where id_producto =  ").append(prod.getId_producto());
            miSQL.append(";");
            System.out.println(miSQL.toString());
            
            this.conn.ejecutarSQl(miSQL.toString());
            editar = true;
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            this.conn.cerrarConexion();
        }
        return editar;
    }
    
}
