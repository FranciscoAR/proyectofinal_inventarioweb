package model;

import java.util.Date;


public class Producto {
    private int id_producto;
    private String nom_producto;
    private String desc_producto;
    private String categoria;
    private int id_categoria;
    private float stock;
    private float precio;
    private String unidadMedida;
    private int estado;
    private Date fecha_entrada;
    
    
    public Producto()
    {
        this.id_producto = 0;
        this.nom_producto = "";
        this.desc_producto = "";
        this.categoria = "";
        this.stock = 0;
        this.precio = 0;
        this.unidadMedida = "";
        this.estado = 0;
        
    }

    public Producto(int id_producto, String nom_producto, String desc_producto, String categoria, float stock, float precio, String unidadMedida, int estado, Date fecha_entrada) {
        this.id_producto = id_producto;
        this.nom_producto = nom_producto;
        this.desc_producto = desc_producto;
        this.categoria = categoria;
        this.stock = stock;
        this.precio = precio;
        this.unidadMedida = unidadMedida;
        this.estado = estado;
        this.fecha_entrada = fecha_entrada;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getNom_producto() {
        return nom_producto;
    }

    public void setNom_producto(String nom_producto) {
        this.nom_producto = nom_producto;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public float getStock() {
        return stock;
    }

    public void setStock(float stock) {
        this.stock = stock;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) { 
        this.estado = estado;
    }

    public String getDesc_producto() {
        return desc_producto;
    }

    public void setDesc_producto(String desc_producto) {
        this.desc_producto = desc_producto;
    }

    public Date getFecha_entrada() {
        return fecha_entrada;
    }

    public void setFecha_entrada(Date fecha_entrada) {
        this.fecha_entrada = fecha_entrada;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

   
    
    
    
}
