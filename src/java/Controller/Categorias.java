package Controller;

import DAO.CategoriaDAO;
import DAO.CategoriaDAOImplementar;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Categoria;


public class Categorias extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
     
    }

 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String parametro = request.getParameter("opcion"); //Capturar el parametro que se esta enviando
        String respuesta = request.getParameter("respuesta");
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        switch (parametro) {
            case "crear":
                this.crearCategorias(request, response);
                break;
            case "listar":
                this.listarCategorias(request, response);
                break;
            case "editar":
                this.editarCat(request, response);
                break;
            case "eliminar":
                this.eliminarCategoria(request, response);
                break;
            case "yes":
                String id = request.getParameter("id");
                System.out.println(id);
            categoria.borrarCat(Integer.parseInt(id));
            this.listarCategorias(request, response);
                break;
            default:
                break;
        }
        
    }

 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        Categoria categoria = new Categoria();
        //Se efectua el castin o conversión de datos porque lo ingresado en el formulario es texto
        categoria.setId_categoria(Integer.parseInt(request.getParameter("id_categoria")));
        categoria.setNom_categoria(request.getParameter("txtNomCategoria"));
        categoria.setEstado_categoria(Integer.parseInt(request.getParameter("txtEstadoCategoria")));
        CategoriaDAO utilidadesCategoria = new CategoriaDAOImplementar();
        utilidadesCategoria.guardarCat(categoria); 
        this.listarCategorias(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void listarCategorias(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html;charset=utf-8");
        //Crear instancia a categoriaDAO
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        //Crear instancia de sesión; se le da true para crear la sesión
        HttpSession sesion = request.getSession();
        sesion.setAttribute("listaCategorias", categoria.Listar());
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/vistas-categorias/listarCategorias.jsp");
        dispatcher.forward(request, response);
    }
    public void crearCategorias(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html;charset=utf-8");
        String pagina = "/vistas-categorias/crearCategoria.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
    }
    
    public void editarCat(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html;charset=utf-8");
        int id = Integer.parseInt(request.getParameter("id"));
        //Crear instancia a categoriaDAO
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        String pagina = "/vistas-categorias/EditarCategoria.jsp";
        //Crear instancia de sesión; se le da true para crear la sesión
        HttpSession sesion = request.getSession();
        sesion.setAttribute("editarCategoria", categoria.editarCat(id));
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
    }
    
    public void eliminarCategoria(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html;charset=utf-8");
        int id = Integer.parseInt(request.getParameter("id"));
        String nombreCategoria = request.getParameter("nombre");
        
        request.setAttribute("id", id);
        request.setAttribute("nombre", nombreCategoria);
        //Crear instancia a categoriaDAO
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        HttpSession sesion = request.getSession();
        sesion.setAttribute("listaProductoEliminar", categoria.productoElimar(id));
       String pagina = "/vistas-categorias/eliminarCategoria.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
    }
}
