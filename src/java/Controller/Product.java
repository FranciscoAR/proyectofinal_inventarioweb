package Controller;

import DAO.ProductoDAO;
import DAO.ProductoDAOImplementar;
import model.Producto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class Product extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String opcion = request.getParameter("opcion");
        
        switch (opcion) {
            case "listar":
                this.listarProducto(request, response);
                break;
            case "crear":
                this.registrarProducto(request, response);
                break;
            case "editar":
                this.editarProd(request, response);
                break;
            case "eliminar":
                this.eliminarCategoria(request, response);
                break;
            case "yes":
                String id = request.getParameter("id");
                ProductoDAO prod = new ProductoDAOImplementar();
                prod.borrarProducto(Integer.parseInt(id));
                this.listarProducto(request, response);
                break;
            default:
                break;
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String opcion = request.getParameter("opcion");
        System.out.println(opcion);
         ProductoDAO produc = new ProductoDAOImplementar();
         Producto prod = new Producto();
        
        if(opcion.equals("actualizarDatos"))
        {
            prod.setId_producto(Integer.parseInt(request.getParameter("id_categoria")));
            prod.setNom_producto(request.getParameter("txtNomProducto"));
            prod.setDesc_producto(request.getParameter("txtDescProducto"));
            prod.setStock(Float.parseFloat(request.getParameter("txtStockProducto")));
            prod.setPrecio(Float.parseFloat(request.getParameter("txtPrecioProducto")));
            prod.setUnidadMedida(request.getParameter("txtUnidadMedidaProducto"));
            prod.setEstado(Integer.parseInt(request.getParameter("txtEstadoProducto")));
            prod.setCategoria(request.getParameter("categoria"));

            produc.actualizarProducto(prod);
        }
        else if(opcion.equals("registrar"))
        {

            prod.setId_producto(Integer.parseInt(request.getParameter("id_categoria")));
            prod.setNom_producto(request.getParameter("txtNomProducto"));
            prod.setDesc_producto(request.getParameter("txtDescProducto"));
            prod.setStock(Float.parseFloat(request.getParameter("txtStockProducto")));
            prod.setPrecio(Float.parseFloat(request.getParameter("txtPrecioProducto")));
            prod.setUnidadMedida(request.getParameter("txtUnidadMedidaProducto"));
            prod.setEstado(Integer.parseInt(request.getParameter("txtEstadoProducto")));
            prod.setCategoria(request.getParameter("categoria"));

           
            produc.registrarProducto(prod);
        }
       
             this.listarProducto(request, response);
        
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void listarProducto(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=utf-8");
        ProductoDAO producto = new ProductoDAOImplementar();
        
          //Crear instancia de sesión; se le da true para crear la sesión
        HttpSession sesion = request.getSession();
        sesion.setAttribute("listaProducto", producto.listar());
        String pagina = "/vistas-producto/listarProductos.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
    }
    
    public void registrarProducto(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=utf-8");
        String pagina = "/vistas-producto/CrearProductos.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
    }
    
     public void eliminarCategoria(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html;charset=utf-8");
        int id = Integer.parseInt(request.getParameter("id"));
        String nombreprod = request.getParameter("nombre");
        
        request.setAttribute("id", id);
        request.setAttribute("nombre", nombreprod);
        
       String pagina = "/vistas-producto/EliminarProducto.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
    }
     
        public void editarProd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html;charset=utf-8");
        int id = Integer.parseInt(request.getParameter("id"));
        //Crear instancia a categoriaDAO
        ProductoDAO producto = new ProductoDAOImplementar();
        String pagina = "/vistas-producto/EditarProducto.jsp";
        //Crear instancia de sesión; se le da true para crear la sesión
        HttpSession sesion = request.getSession();
        sesion.setAttribute("editarProducto", producto.editarProducto(id));
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
    }}
