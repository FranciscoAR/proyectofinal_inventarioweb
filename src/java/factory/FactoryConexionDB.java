package factory;


public class FactoryConexionDB {
    //Podemos definir la configuración para diferentes base de datos
    public static final int MYSQL = 1;
    public static final String[] configMysql = {"db_inventario", "root", ""}; //Modificar el db_inventario luego
    
    public static ConexionDB open(int tipoDB)
    {
        switch(tipoDB){
            case FactoryConexionDB.MYSQL:
                return new MySQLConexionFactory(configMysql);
            default: 
                return null;
                
        }
    }
}
