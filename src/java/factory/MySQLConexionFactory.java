package factory;

import java.sql.*;


public final class MySQLConexionFactory extends ConexionDB{

    public MySQLConexionFactory(String[] Criterios)
    {
        this.parametros = Criterios; // Pasar los criterioa al array de la clase padre, parametros
        this.open(); // Llamar al método open()
    }
    
    //Implementar el método open
    @Override
    public Connection open() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            //En this.parametros[] iran los datos respectivos de nombre de la BD, usuario y clave
            this.conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + this.parametros[0], this.parametros[1], this.parametros[2]);
        }catch(Exception ex)
        {
            ex.printStackTrace(); //Utilizado para mostrar el nombre de la excepcion junto a su mensaje
        }
        return this.conexion;
    }
    
}
