package test;

import DAO.*;
import model.Categoria;
import java.util.*;
import model.Producto;
import model.Usuario;

public class prueba {
    public static void main(String[] args) {
        prueba evaluar = new prueba();
        System.out.println("Archivo de pruebas unitarias ");
        //evaluar.guardarCategoria();
        //evaluar.listarCategorias();
        //evaluar.borrarCategoria();
        //evaluar.listarCategorias();
        //evaluar.iniciarsesion();
        //evaluar.EditarCategoria();
        //evaluar.correo();
        //evaluar.contraseña();
        //evaluar.modificarcontraseña();
        //evaluar.contraseña();
        //evaluar.pregunta();
        //evaluar.respuesta();
        //evaluar.listarProducti();
        //evaluar.verificarCorreo();
        evaluar.prodEliminar();
    }
    
    public void listarCategorias()
    {
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        List<Categoria> listar = categoria.Listar();
        System.out.println("LISTADO DE CATEGORIAS");
        for(Categoria categoriaListar : listar)
        {
            System.out.println("ID: " + categoriaListar.getId_categoria() + 
                    " NOMBRE: " + categoriaListar.getNom_categoria() +
                    " ESTADO: " + categoriaListar.getEstado_categoria());
        }
    }
    
    public void listarProducti()
    {
        ProductoDAO prod = new ProductoDAOImplementar();
        List<Producto> listar = prod.listar();
        for(Producto prodlistar : listar)
        {
            System.out.println("Nombre de producto" + prodlistar.getNom_producto());
        }
    }
    
        public void prodEliminar()
    {
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        List<Producto> listar = categoria.productoElimar(1008);
        for(Producto prodEliminar : listar)
        {
            System.out.println(prodEliminar.getNom_producto());
            System.out.println(prodEliminar.getStock());
            System.out.println(prodEliminar.getEstado());
        }
        
    }
    
    public void iniciarsesion()
    {
        UsuarioDAO usuario = new UsuarioDAOImplementar();
        if (usuario.startSesion("fran@gmail.com", "123456").size() > 0)
        {
            System.out.println("|Bienvenido al sistema");
        }
        
    }
    
    public void EditarCategoria()
    {
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        Categoria cat_edit = categoria.editarCat(1000);
        System.out.println("CATEGORIA A MODIFICAR");
        System.out.println("ID: " + cat_edit.getId_categoria() +
                "NOMBRE: " + cat_edit.getNom_categoria() +
                "ESTADO: " + cat_edit.getEstado_categoria());
    }
    
    public void guardarCategoria(){
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        Categoria guardar_cat = new Categoria();
        guardar_cat.setId_categoria(1010);
        guardar_cat.setNom_categoria("Muebles Hogareños");
        guardar_cat.setEstado_categoria(1);
        categoria.guardarCat(guardar_cat);
    }
    public void borrarCategoria()
    {
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        categoria.borrarCat(1009);
    }
    
 
    public void correo()
    {
        UsuarioDAO usuario = new UsuarioDAOImplementar();
        System.out.println(usuario.recuperarCorreo(78902134));
    }
    
    public void contraseña()
    {
        UsuarioDAO usuario = new UsuarioDAOImplementar();
        System.out.println(usuario.recuperarContraseña("neftali@gmail.com"));
    }
    
    public void modificarcontraseña()
    {
        UsuarioDAO usuario = new UsuarioDAOImplementar();
        usuario.modificarContraseña("neftali@gmail.com", "1234567", "12345678");
    }
    
    public void pregunta()
    {
        UsuarioDAO usuario = new UsuarioDAOImplementar();
        System.out.println(usuario.pregunta("fran@gmail.com"));
    }
    
    public void respuesta()
    {
         UsuarioDAO usuario = new UsuarioDAOImplementar();
         System.out.println(usuario.respuesta("fran@gmail.com", "segundo nombre?"));
    }
    
    public void verificarCorreo()
    {
        UsuarioDAO usuario = new UsuarioDAOImplementar();
        System.out.println(usuario.verificarCorreo("fran@gmail.com"));
    }
    
   

}
