<%@page import="model.Categoria"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.CategoriaDAOImplementar"%>
<%@page import="DAO.CategoriaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="editarProducto" scope="session" class="model.Producto" />
<% 
   try{
        HttpSession var_Session = request.getSession(false);
        String nombres = (String) var_Session.getAttribute("nombre_Completo");
        String user = (String) var_Session.getAttribute("nombreUsuario");
        String tipo = (String) var_Session.getAttribute("usuario_tipo");
        String correo = (String) var_Session.getAttribute("correo_usuario");
 
        if(user == null){
            String url = "../";   
                        out.println("<script>valor=confirm(\"No ha iniciado session.  \\n\\nClic en aceptar para redrigirse a iniciar session. \");valor;"
                        + "if (valor==true){"
                        + "location.href='"+ url + "';"
                        + "}else{"
                        + "location.href='"+ url + "';"
                        + "}"
                        + "</script>");
        }else if(user!=null){
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar producto</title>
        <%@include file = "../WEB-INF/vistas-parciales/css-js.jspf" %>
        <script type="text/javascript">
            function regresar(url){ //Función para el botón regresar
                location.href = url;
            }
        </script>
    </head>
    <body background="assets/img/fondo.jpg">
         <!-- Vista parcial de la parte superior de nuestra aplicación -->
        <%@include file= "../WEB-INF/vistas-parciales/encabezado.jspf" %>
        
        <style>
            body{
                padding: 1%;
            }
          form{
              width: 490px;
              background: #000;
              padding: 30px;
              margin: auto;
              margin-top: 0px;
              border-radius: 5px;
              font-family: 'calibri';
              color: white;
              box-shadow: 30px 30px 500px #000; 
          }
            
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none; 
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
      
    
  <div class="container custom-container">
      <div class="row h-100">
        <div class="col-sm-12 col-lg-12 d-flex justify-content-center align-items-center">
            
              
        <form class="form-horizontal" id="formCategoria" name="frmCategoria" action="<%= request.getContextPath() %>/producto.go" method="POST">
            
            <img class="rounded mx-auto d-block" src="assets/img/IconoEditarProducto.png" alt="" width="100" height="100"><br>
            <h3 align="center"> Editar producto </h3><br>
            <input type="hidden" name="opcion" value="actualizarDatos">
           <div class="form-group">
               <label for="txtIDproducto" class="control-label">ID Producto:</label> 
                <div class="col-sm-12">
            <input type="text" class="form-control" name="id_categoria" value="<%= editarProducto.getId_producto() %>" readonly="true" required>
               </div>
            </div>
            <div class="form-group">
                <label for="txtnombreproducto" class="control-label">Nombre Producto:</label> 
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtNomProducto" value="<%= editarProducto.getNom_producto() %>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtDescProducto" class="control-label" >Descripcion de producto:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtDescProducto" value="<%= editarProducto.getDesc_producto() %>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtStockProducto" class="control-label" >Stock de producto:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtStockProducto" value="<%= editarProducto.getStock() %>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtPrecioProducto" class="control-label" >Precio de producto:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtPrecioProducto" value="<%= editarProducto.getPrecio() %>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtUnidadMedidaProducto" class="control-label" >Unidad de medida de producto:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtUnidadMedidaProducto" value="<%= editarProducto.getUnidadMedida() %>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtEstadoProducto" class="control-label" >Estado de producto:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtEstadoProducto" value="<%= editarProducto.getEstado() %>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtCategoriaProducto" class="control-label" >Categoria de producto:</label>
                <div class="col-sm-12">
                    <select class="form-select" aria-label="Default select example" name="categoria">
                        <option selected value="<%= editarProducto.getId_categoria()%>"><%= editarProducto.getCategoria() %></option>
                        <%
                            CategoriaDAO categoria = new CategoriaDAOImplementar();
                            List<Categoria> cat = new ArrayList<Categoria>();
                            
                            cat = categoria.Listar();
                            for(int i = 0; i < cat.size() ; i++)
                            {
                               Categoria categ = cat.get(i);
                        %>
                        <option value="<%=  categ.getId_categoria() %>"><%= categ.getNom_categoria() %></option>
                        <%
                            }
                        %>
                  </select>
                </div>
            </div>
                  <br>
            <div class="form-group" align='center'>
                <div class="col-sm-offset-2 col-sm-8">
                    <input type="submit" class="btn btn-success btn-lg" name="btnGuardar" value="Actualizar" />
                    <input type="button" class="btn btn-danger btn-lg" onclick="regresar('<%= request.getContextPath() %>/producto.go?opcion=listar')"
                           name="btnRegresar" value="Regresar" />
                </div>
            </div><br>
        <div class="form-group" align='center'>
                <div class="col-sm-offset-2 col-sm-8">
          <!-- Vista parcial de la parte inferior de nuestra aplicación -->
        <%@include file="../WEB-INF/vistas-parciales/pie.jspf" %>
        </div>
        </form>
    </body>
</html>
<%
            }
               
            }catch(Exception e){

            }
   %>
