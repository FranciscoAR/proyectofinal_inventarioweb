<%@page import="model.Categoria"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="DAO.CategoriaDAOImplementar"%>
<%@page import="DAO.CategoriaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="producto" scope="session" class="model.Producto" />
<% 
   try{
        HttpSession var_Session = request.getSession(false);
        String nombres = (String) var_Session.getAttribute("nombre_Completo");
        String user = (String) var_Session.getAttribute("nombreUsuario");
        String tipo = (String) var_Session.getAttribute("usuario_tipo");
        String correo = (String) var_Session.getAttribute("correo_usuario");
 
        if(user == null){
            String url = "../";   
                        out.println("<script>valor=confirm(\"No ha iniciado session.  \\n\\nClic en aceptar para redrigirse a iniciar session. \");valor;"
                        + "if (valor==true){"
                        + "location.href='"+ url + "';"
                        + "}else{"
                        + "location.href='"+ url + "';"
                        + "}"
                        + "</script>");
        }else if(user!=null){
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro de productos</title>
        <%@include file = "../WEB-INF/vistas-parciales/css-js.jspf" %>
        <script type="text/javascript">
            function regresar(url){ //Función para el botón regresar
                location.href = url;
            }
        </script>
    </head>
    <body>
         <!-- Vista parcial de la parte superior de nuestra aplicación -->
        <%@include file= "../WEB-INF/vistas-parciales/encabezado.jspf" %>
        <h3> Registrar producto </h3>      
        <form class="form-horizontal" id="formCategoria" name="frmCategoria" action="<%= request.getContextPath() %>/producto.go" method="POST">
           <div class="form-group">
               <input type="hidden" name="opcion" value="registrar">
               <label for="txtIDproducto" class="col-sm-2 control-label">ID Producto:</label> 
                <div class="col-sm-10">
            <input type="text" class="form-control" name="id_categoria" value="<%= producto.getId_producto() %>" required>
               </div>
            </div>
            <div class="form-group">
                <label for="txtnombreproducto" class="col-sm-2 control-label">Nombre Producto:</label> 
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="txtNomProducto" value="<%= producto.getNom_producto() %>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtDescProducto" class="col-sm-2 control-label" >Descripcion de producto:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="txtDescProducto" value="<%= producto.getDesc_producto() %>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtStockProducto" class="col-sm-2 control-label" >Stock de producto:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="txtStockProducto" value="<%= producto.getStock() %>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtPrecioProducto" class="col-sm-2 control-label" >Precio de producto:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="txtPrecioProducto" value="<%= producto.getPrecio() %>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtUnidadMedidaProducto" class="col-sm-2 control-label" >Unidad de medida de producto:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="txtUnidadMedidaProducto" value="<%= producto.getUnidadMedida() %>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtEstadoProducto" class="col-sm-2 control-label" >Estado de producto:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="txtEstadoProducto" value="<%= producto.getEstado() %>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtCategoriaProducto" class="col-sm-2 control-label" >Categoria de producto:</label>
                <div class="col-sm-10">
                    <select class="form-select" aria-label="Default select example" name="categoria">
                        <option selected>Seleccione una categoria</option>
                        <%
                            CategoriaDAO categoria = new CategoriaDAOImplementar();
                            List<Categoria> cat = new ArrayList<Categoria>();
                            
                            cat = categoria.Listar();
                            for(int i = 0; i < cat.size() ; i++)
                            {
                               Categoria categ = cat.get(i);
                        %>
                        <option value="<%=  categ.getId_categoria() %>"><%= categ.getNom_categoria() %></option>
                        <%
                            }
                        %>
                  </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" class="btn btn-success btn-sm" name="btnGuardar" value="Guardar" />
                    <input type="button" class="btn btn-danger btn-sm" onclick="regresar('<%= request.getContextPath() %>/producto.go?opcion=listar')"
                           name="btnRegresar" value="Regresar" />
                </div>
            </div>
        </form>
        
          <!-- Vista parcial de la parte inferior de nuestra aplicación -->
        <%@include file="../WEB-INF/vistas-parciales/pie.jspf" %>
    </body>
</html>
<%
            }
               
            }catch(Exception e){

            }
   %>
