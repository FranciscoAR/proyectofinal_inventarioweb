<%@page import="model.Producto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    ////request.getContextPath()
    String id_cat = request.getParameter("id");
    String nombre_cat = request.getParameter("nombre");
%>
<% 
   try{
        HttpSession var_Session = request.getSession(false);
        String nombres = (String) var_Session.getAttribute("nombre_Completo");
        String user = (String) var_Session.getAttribute("nombreUsuario");
        String tipo = (String) var_Session.getAttribute("usuario_tipo");
        String correo = (String) var_Session.getAttribute("correo_usuario");
 
        if(user == null){
            String url = "../";   
                        out.println("<script>valor=confirm(\"No ha iniciado session.  \\n\\nClic en aceptar para redrigirse a iniciar session. \");valor;"
                        + "if (valor==true){"
                        + "location.href='"+ url + "';"
                        + "}else{"
                        + "location.href='"+ url + "';"
                        + "}"
                        + "</script>");
        }else if(user!=null){
%>
<jsp:useBean id="listaProductoEliminar" scope="session" class="java.util.List" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Baja Categorias</title>
         <%@include file = "../WEB-INF/vistas-parciales/css-js.jspf" %>
    </head>
    <body background="assets/img/fondo.jpg">
        <!-- Vista parcial de la parte superior de nuestra aplicación -->
        <%@include file= "../WEB-INF/vistas-parciales/encabezado.jspf" %>
        
        <style>
         
          
          form{
              width: 490px;
              background: #000;
              padding: 35px;
              margin: auto;
              margin-top: 120px;
              border-radius: 4px;
              font-family: 'calibri';
              color: white;
              box-shadow: 30px 30px 500px #000; 
          }
            
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none; 
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    
    <form>
        <div class="col-auto bg-gray p-5 text-center">
            <img class="rounded mx-auto d-block" src="assets/img/IconoEliminar.png" alt="" width="100" height="100"><br>
            <h2>Eliminar Categorias</h2><br>
            
            <h4>¿Realmente Desea Eliminar el Registro?</h4>
            <h5>ID Categoria: <%= id_cat %></h5>
            <h5>Nombre Categoria: <%= nombre_cat %></h5>
        </div>
        <div>
            <h4>Eliminara estos productos al mismo tiempo</h4>
            <table class="table  table-dark table-hover">
                <tr>
                    <th>
                        PRODUCTO
                    </th>
                    <th>
                        STOCK
                    </th>
                    <th>
                        ESTADO
                    </th>
                 
                </tr>
                    <%
                    for(int i = 0; i < listaProductoEliminar.size(); i++)
                    {
                    Producto prod = new Producto();
                    prod = (Producto)listaProductoEliminar.get(i);
                    
                    %>
                    <tr>
                        <td><%= prod.getNom_producto()%></td>
                        <td><%= prod.getStock()%></td>
                        <td><%= prod.getEstado() %></td>
                       
                    </tr>
                    <%
                        }
                    %>
                    
            </table>
        </div>
        
        <!---->
        <div class="form-group" align='center'>
                <div class="col-sm-offset-2 col-sm-8">
            <a href="categorias.go?opcion=yes&&id=<%= id_cat %>" class="btn btn-primary btn-lg" role="button">Si / Aceptar</a>
            <!--<a href="< //request.getContextPath() >" class="btn btn-secondary btn-lg" role="button">No / Cancelar</a>-->
            <a href="categorias.go?opcion=listar" class="btn btn-secondary btn-lg" role="button">No / Cancelar</a>
            
            </div>
            <br>
            <!-- Vista parcial de la parte inferior de nuestra aplicación -->
        <%@include file="../WEB-INF/vistas-parciales/pie.jspf" %>
        
        </div>
</form>
    </body>
</html>
<%
            }
               
            }catch(Exception e){

            }
   %>
