<%@page import="model.Categoria"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
   try{
        HttpSession var_Session = request.getSession(false);
        String nombres = (String) var_Session.getAttribute("nombre_Completo");
        String user = (String) var_Session.getAttribute("nombreUsuario");
        String tipo = (String) var_Session.getAttribute("usuario_tipo");
        String correo = (String) var_Session.getAttribute("correo_usuario");
 
        if(user == null){
            String url = "../";   
                        out.println("<script>valor=confirm(\"No ha iniciado session.  \\n\\nClic en aceptar para redrigirse a iniciar session. \");valor;"
                        + "if (valor==true){"
                        + "location.href='"+ url + "';"
                        + "}else{"
                        + "location.href='"+ url + "';"
                        + "}"
                        + "</script>");
        }else if(user!=null){
%>
<!DOCTYPE html>
<!-- El id debe ser el mismo que se le colocó de nombre a la session en el controlador -->
<jsp:useBean id="listaCategorias" scope="session" class="java.util.List" />

<html>
           <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <title>
               Control de inventario
           </title>
        <%@include file = "../WEB-INF/vistas-parciales/css-js.jspf" %>
    </head>
    <body background="assets/img/fondo.jpg">
        <!-- Vista parcial de la parte superior de nuestra aplicación -->
        <%@include file= "../WEB-INF/vistas-parciales/encabezado.jspf" %>
        
        <style>
          
          form{
              width: 100%;
              background-color:  rgba(0, 0, 0,.6);
              padding: 30px;
              margin: auto;
              margin-top: 100px;
              border-radius: 4px;
              font-family: 'calibri';
              color: white;
              box-shadow: -40px 40px 50px #000,
                           40px 40px 50px #000;
          }
          table{
              background-color:  rgba(0, 0, 0,.4);
              padding: 3px;
              margin: auto;
              margin-top: 10px;
              border-radius: 4px;
              font-family: 'calibri';
              color: white;
              align-content: center;
              position: center;
          }
          .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none; 
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
          
          </style>
        
          <div class="container custom-container">
        <div class="col-sm-12 col-lg-12 col-xs-12 d-flex justify-content-center align-items-center">
        
        <form>
        <h2 align="center">Bienvenid@ a!</h2>
        <div>
            <h3 align="center">Listado de Categorias Registradas</h3><br>
            <a href="<%= request.getContextPath() %>/categorias.go?opcion=crear" class="btn btn-success btn-lg glyphicon glyphicon-pencil" role="button"> Nueva Categoria</a>
            <center>
            <table class="table  table-dark table-hover">
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        NOMBRE
                    </th>
                    <th>
                        ESTADO
                    </th>
                    <th>
                        ACCIÓN
                    </th>
                </tr>
                
                    <%
                    for(int i = 0; i < listaCategorias.size(); i++)
                    {
                    Categoria categoria = new Categoria();
                    categoria = (Categoria)listaCategorias.get(i);
                    
                    %>
                    <tr>
                        <td><%= categoria.getId_categoria()%></td>
                        <td><%= categoria.getNom_categoria()%></td>
                        <td><%= categoria.getEstado_categoria()%></td>
                        <td>
                            <a href="<%= request.getContextPath() %>/categorias.go?opcion=editar&id=<%= categoria.getId_categoria()%>" class="btn btn-info btn-sm glyphicon glyphicon-edit" role="button">Editar</a>
                            <a href="<%= request.getContextPath() %>/categorias.go?opcion=eliminar&id=<%= categoria.getId_categoria()%>&nombre=<%= categoria.getNom_categoria()%>" class="btn btn-danger btn-sm glyphicon glyphicon-remove" role="button">Eliminar</a>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                    
            </table>
                    <!-- Vista parcial de la parte inferior de nuestra aplicación -->
        <%@include file="../WEB-INF/vistas-parciales/pie.jspf" %>    
        </div>
                
            </div>
                  <br>
              <br>  
                    
                    </form>
    </body>
</html>
<%
            }
               
            }catch(Exception e){

            }
   %>

