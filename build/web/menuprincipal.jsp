<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
   try{
        HttpSession var_Session = request.getSession(false);
        String nombres = (String) var_Session.getAttribute("nombre_Completo");
        String user = (String) var_Session.getAttribute("nombreUsuario");
        String tipo = (String) var_Session.getAttribute("usuario_tipo");
        String correo = (String) var_Session.getAttribute("correo_usuario");
 
        if(user == null){
            String url = "./";   
                        out.println("<script>valor=confirm(\"No ha iniciado session.  \\n\\nClic en aceptar para redrigirse a iniciar session. \");valor;"
                        + "if (valor==true){"
                        + "location.href='"+ url + "';"
                        + "}else{"
                        + "location.href='"+ url + "';"
                        + "}"
                        + "</script>");
        }else if(user!=null){
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            Inicio
        </title>
        <%@include file = "WEB-INF/vistas-parciales/css-js.jspf" %>
    </head>
    <body background="assets/img/fondo.jpg">
        <!-- Vista parcial de la parte superior de nuestra aplicación -->
        <%@include file= "WEB-INF/vistas-parciales/encabezado.jspf" %>
        
        <style>
          form{
              width: 660px;
              background-color:  rgba(0, 0, 0,.6);
              padding: 30px;
              margin: auto;
              margin-top: 150px;
              border-radius: 4px;
              font-family: 'calibri';
              color: white;
              box-shadow: -40px 40px 50px #000,
                           40px 40px 50px #000;
          }
          .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none; 
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
          
          </style>
          
          <div class="container custom-container">
        <div class="col-sm-12 col-lg-12 col-xs-12 d-flex justify-content-center align-items-center">
        
                <form>
          <div class="text-bg- p-3 text-center"><h1>Bienvenid@ <%= nombres %></h1>
        
        <div class="col-md-auto">
                    <!-- Vista parcial de la parte inferior de nuestra aplicación -->
       <%@include file="WEB-INF/vistas-parciales/pie.jspf" %>
            </div>
          </div>
          </form>
    </body>
</html>
<%
            }
               
            }catch(Exception e){

            }
   %>
