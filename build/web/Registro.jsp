<%@page import="DAO.UsuarioDAOImplementar"%>
<%@page import="DAO.UsuarioDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="Usuario" scope="session" class="model.Usuario" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Crear cuenta</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <%@include file = "WEB-INF/vistas-parciales/css-js.jspf" %>
        <script type="text/javascript">
            function regresar(url){ //Función para el botón regresar
                location.href = url;
            }
        </script>
    </head>
    <body background="assets/img/fondo.jpg">
        <style>
          body{
            margin-top: 6%;
            margin-bottom: 3%;
        }
          form{
              width: 600px;
              background: #000;
              padding: 30px;
              margin: auto;
              margin-top: 70px;
              margin-bottom: 50px;
              border-radius: 5px;
              font-family: 'calibri';
              color: white;
              box-shadow: 30px 30px 500px #000; 
          }
            
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none; 
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
      
  <div class="container custom-container">
        <div class="col-sm-12 col-lg-12 col-xs-12 d-flex justify-content-center align-items-center">
              
        <form class="form-horizontal" id="formRegUsuario" name="frmCategoria" action="<%= request.getContextPath() %>/loggin.go" method="POST">
            <img class="rounded mx-auto d-block" src="assets/img/IconoNuevaCuenta.png" alt="" width="100" height="100">
            <center><h2>Registrar Usuario</h2></center><br>
            <%
               
                String aviso = (String)request.getAttribute("aviso");
                System.out.println("aviso");
                
                if(aviso.equals("nuevo"))
                {
                
                }
                else if (aviso.equals("coincidenciaCorreo"))
                {
                 String mensaje = (String)request.getParameter("Message");
                 System.out.println(mensaje);
            %>
            <h4><b>Correo elctronico ya en uso, por favor utilice un correo nuevo</b></h4>
            <%
                }
            %>
            <input type="hidden" name="id_Usuario" value="<%= Usuario.getId() %>" >
            <input type="hidden" name="opcion" value="registrar">
            <div class="form-group">
                <label for="txtNomUsuario" class="col-sm-2 control-label">Nombre:</label> 
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtNomUsuario" value="<%= Usuario.getNombre() %>" placeholder="Ingrese su nombre" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtApellidoUsuario" class="col-sm-2 control-label">Apellido:</label> 
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtApellidoUsuario" value="<%= Usuario.getApellido() %>" placeholder="Ingrese su apellido" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtCorreoUsuario" class="col-sm-2 control-label">Correo:</label> 
                <div class="col-sm-12">
                    
                    <input type="email" class="form-control" id="correoUsuario" name="txtCorreoUsuario" value="<%= Usuario.getCorreo() %>" placeholder="DirecciÃ³n de correo electronico"
                           required >
                </div>
            </div>
            <div class="form-group">
                <label for="txtUsuUsuario" class="col-sm-2 control-label">Usuario:</label> 
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtUsuUsuario" value="<%= Usuario.getUsuario()%>" placeholder="Ingrese su usuario" required >
                </div>
            </div>
            <div class="form-group">
                <label for="txtClaveUsuario" class="col-sm-2 control-label">Clave:</label> 
                <div class="col-sm-12">
                    <input type="password" class="form-control" name="txtClaveUsuario" value="<%= Usuario.getClave()%>" placeholder="ContraseÃ±a" required >
                </div>
            </div>
            <div class="form-group">
                <label for="txtTelefonoUsuario" class="col-sm-2 control-label">Telefono:</label> 
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtTelefonoUsuario"  placeholder="Introduzca su número de telefono" required >
                </div>
            </div>
            <div class="form-group">
                <label for="txtEstadoUsuario" class="col-sm-2 control-label">Estado:</label> 
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtEstadoUsuario" value="<%= Usuario.getEstado() %>" placeholder="Ingrese 1 para Activo " required >
                </div>
            </div>
            <div class="form-group">
                <label for="txtTipoUsuario" class="col-sm-2 control-label">Tipo:</label> 
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtTipooUsuario" value="<%= Usuario.getTipo() %>" placeholder="Ingrese su tipo " required >
                </div>
            </div> 
            <div class="form-group">
                <label for="txtPreguntaUsuario" class="col-sm-2 control-label">Pregunta:</label> 
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtPreguntaUsuario" value="<%= Usuario.getPregunta() %>" placeholder="Ingrese su pregunta para recuperaciÃ³n de contraseÃ±a" required >
                </div>
            </div>
            <div class="form-group">
                <label for="txtRespuestaUsuario" class="col-sm-2 control-label">Respuesta:</label> 
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="txtRespuestaUsuario" value="<%= Usuario.getRespuesta() %>" placeholder="Ingrese la respuesta a su pregunta " required >
                </div>
                <br>
            </div>
            <div class="form-group" align='center'>
                <div class="col-sm-offset-2 col-sm-8">
                    <input type="submit" class="btn btn-success btn-lg" name="btnRegistrar" value="Crear cuenta" />
                    <input type="button" class="btn btn-danger btn-lg" onclick="regresar('<%= request.getContextPath() %>/index.jsp')"
                           name="btnRegresar" value="Regresar" />
                </div>
                           <br>
            </div>
                           <div class="col-md-auto">
       <%@include file="WEB-INF/vistas-parciales/pie.jspf" %>
            </div>
        </form>
            
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

    </body>
</html>
