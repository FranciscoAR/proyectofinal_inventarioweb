<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.Producto"%>
<% 
   try{
        HttpSession var_Session = request.getSession(false);
        String nombres = (String) var_Session.getAttribute("nombre_Completo");
        String user = (String) var_Session.getAttribute("nombreUsuario");
        String tipo = (String) var_Session.getAttribute("usuario_tipo");
        String correo = (String) var_Session.getAttribute("correo_usuario");
 
        if(user == null){
            String url = "../";   
                        out.println("<script>valor=confirm(\"No ha iniciado session.  \\n\\nClic en aceptar para redrigirse a iniciar session. \");valor;"
                        + "if (valor==true){"
                        + "location.href='"+ url + "';"
                        + "}else{"
                        + "location.href='"+ url + "';"
                        + "}"
                        + "</script>");
        }else if(user!=null){
%>

<!DOCTYPE html>
<jsp:useBean id="listaProducto" scope="session" class="java.util.List" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Productos Ingresados</title>
        <%@include file = "../WEB-INF/vistas-parciales/css-js.jspf" %>
    </head>
    <body background="assets/img/fondo.jpg">
        <%@include file= "../WEB-INF/vistas-parciales/encabezado.jspf" %>
        
        <style>
            body{
                padding: 2%;
            }
          
          form{
              width: 1000%;
              background-color:  rgba(0, 0, 0,.6);
              padding: 30px;
              margin: auto;
              margin-top: 20px;
              border-radius: 4px;
              font-family: 'calibri';
              color: white;
              box-shadow: -40px 40px 50px #000,
                           40px 40px 50px #000;
          }
          table{
              background-color:  rgba(0, 0, 0,.4);
              padding: 3px;
              margin: auto;
              margin-top: 10px;
              border-radius: 4px;
              font-family: 'calibri';
              color: white;
              align-content: center;
              position: center;
          }
          .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none; 
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
          
          </style>
        
          <div class="container custom-container">
        <div class="col-sm-12 col-lg-12 col-xs-12 d-flex justify-content-center align-items-center">
            
                <form>
        <h2 align="center">Bienvenid@ a</h2>
            <h3 align="center">Listado de Productos Registrados</h3><br>
        <br>
        <div>
        <table class="table table-dark table-hover">
            <thead>
                <tr >
                <th scope="col">ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripción</th>
                <th scope="col">Categortia</th>
                <th scope="col">Stock</th>
                <th scope="col">Precio</th>
                <th scope="col">Unidad de medida</th>
                <th scope="col">Estado</th>
                <th scope="col">Fecha registro</th>
                <th scope="col" colspan="2">Acciones</th>
              </tr>
            </thead>
            <tbody>
                <%
                    
                    for(int i = 0; i < listaProducto.size(); i ++)
                    {
                    Producto prod = new Producto();
                    prod = (Producto)listaProducto.get(i);
                %>
              <tr>
                  <th scope="row"><%= prod.getId_producto() %></th>
                  <td><%= prod.getNom_producto() %></td>
                  <td><%= prod.getDesc_producto() %></td>
                  <td><%= prod.getCategoria() %></td>
                  <td><%= prod.getStock() %></td>
                  <td>$<%= prod.getPrecio() %></td>
                  <td><%= prod.getUnidadMedida() %></td>
                  <td><%= prod.getEstado() %></td>
                  <td><%= prod.getFecha_entrada() %></td>
                  <td colspan="2">
                    <a href="<%= request.getContextPath() %>/producto.go?opcion=editar&id=<%= prod.getId_producto() %>" class="btn btn-info btn-sm glyphicon glyphicon-edit" role="button">Editar</a>
                    <a href="<%= request.getContextPath() %>/producto.go?opcion=eliminar&id=<%= prod.getId_producto()%>&nombre=<%= prod.getNom_producto() %>" class="btn btn-danger btn-sm glyphicon glyphicon-remove" role="button">Eliminar</a>
                  </td>
                
              </tr>
              <% 
                  }
              %>
            </tbody>
        </table>
        <center>
        <a class="btn btn-primary btn-lg" href="producto.go?opcion=crear" role="button">Registrar nuevo producto</a>
        <%@include file="../WEB-INF/vistas-parciales/pie.jspf" %>
        </div>
        </div>
      </div>
         
                  <br>
              <br>  
                </form>
    </body>
</html>
<%
            }
               
            }catch(Exception e){

            }
   %>
