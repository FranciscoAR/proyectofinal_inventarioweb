<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
   try{
        HttpSession var_Session = request.getSession(false);
        String nombres = (String) var_Session.getAttribute("nombre_Completo");
        String user = (String) var_Session.getAttribute("nombreUsuario");
        String tipo = (String) var_Session.getAttribute("usuario_tipo");
        String correo = (String) var_Session.getAttribute("correo_usuario");
 
        if(user == null){
            String url = "../";   
                        out.println("<script>valor=confirm(\"No ha iniciado session.  \\n\\nClic en aceptar para redrigirse a iniciar session. \");valor;"
                        + "if (valor==true){"
                        + "location.href='"+ url + "';"
                        + "}else{"
                        + "location.href='"+ url + "';"
                        + "}"
                        + "</script>");
        }else if(user!=null){
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar contraseña</title>
        
        <%@include file = "WEB-INF/vistas-parciales/css-js.jspf" %>
    </head>
    <body background="assets/img/fondo.jpg">
         <!-- Vista parcial de la parte superior de nuestra aplicación -->
        <%@include file= "WEB-INF/vistas-parciales/encabezado.jspf" %>
        <br>
        
        <style>
          
          form{
              width: 490px;
              background: #000;
              padding: 40px;
              margin: auto;
              margin-top: 20px;
              margin-bottom: 50px;
              border-radius: 4px;
              font-family: 'calibri';
              color: white;
              box-shadow: 30px 30px 500px #000; 
          }
            
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none; 
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
      
    
  <div class="container custom-container">
        <div class="col-sm-12 col-lg-12 col-xs-12 d-flex justify-content-center align-items-center">
                
        <div> 
            <form action="loggin.go" method="post" >
                
                <img class="rounded mx-auto d-block" src="assets/img/IconoModificarContraseña.png" alt="" width="100" height="100"><br>
                <center><h3>Modificación de contraseña!</h3></center><br>
                <input type="hidden" name="opcion" value="modificarContrasena" />
                <input type="hidden" name="correoUsuario" value="<%= correo %>" />
                <div class="form-group">
                    <label for="contraseñaActual">Ingrese su contraseña actual: </label>
                    <input type="password" class="form-control" name="txtClaveUsuario"  id="contraseñaActual" placeholder="Ingrese su contraseña actual" required>
                    
                    <label for="contraseñaActual">Ingrese su nueva contraseña: </label>
                    <input type="password" class="form-control" name="txtNuevaClaveUsuario"  id="NuevaContraseña" placeholder="Ingrese su contraseña actual" required>
                    
                    <label for="contraseñaActual">Ingrese nuevamente su contraseña: </label>
                    <input type="password" class="form-control" name="txtNuevaClaveUsuario2"  id="NuevaContraseña2" 
                           placeholder="Ingrese su contraseña actual"  onchange="if(NuevaContraseña.value === NuevaContraseña2.value)
                            {}
                            else {alert('Contraseñas no coinciden'); NuevaContraseña.value=''; NuevaContraseña2.value='';} "required><br>
                    
                    <center><button type="submit" class="btn btn-info btn-lg">Modificar contraseña</button></center>
                </div>
                <div> 
                    
                </div><br>
                <div class="col-md-auto">
                    <!-- Vista parcial de la parte inferior de nuestra aplicación -->
       <%@include file="WEB-INF/vistas-parciales/pie.jspf" %>
            </div>
            </form>
        </div>
    </body>
</html>
<%
            }
               
            }catch(Exception e){

            }
   %>
