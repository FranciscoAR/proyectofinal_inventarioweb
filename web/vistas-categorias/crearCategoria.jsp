<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Para importar los datos de la clase categoria -->
<jsp:useBean id="categoria" scope="session" class="model.Categoria" />
<% 
   try{
        HttpSession var_Session = request.getSession(false);
        String nombres = (String) var_Session.getAttribute("nombre_Completo");
        String user = (String) var_Session.getAttribute("nombreUsuario");
        String tipo = (String) var_Session.getAttribute("usuario_tipo");
        String correo = (String) var_Session.getAttribute("correo_usuario");
 
        if(user == null){
            String url = "../";   
                        out.println("<script>valor=confirm(\"No ha iniciado session.  \\n\\nClic en aceptar para redrigirse a iniciar session. \");valor;"
                        + "if (valor==true){"
                        + "location.href='"+ url + "';"
                        + "}else{"
                        + "location.href='"+ url + "';"
                        + "}"
                        + "</script>");
        }else if(user!=null){
%>
<!DOCTYPE html>
<html>
  <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            Control de Inventario
        </title>
        <%@include file = "../WEB-INF/vistas-parciales/css-js.jspf" %>
        <script type="text/javascript">
            function regresar(url){ //Función para el botón regresar
                location.href = url;
            }
        </script>
    </head>
    <body>
        <!-- Vista parcial de la parte superior de nuestra aplicación -->
        <%@include file= "../WEB-INF/vistas-parciales/encabezado.jspf" %>
        
        <style>
          body{
              background-image: url('assets/img/fondo.jpg');
              background-size: 120%;
              background-repeat: no-repeat;
          }
          form{
              width: 490px;
              background: #000;
              padding: 40px;
              margin: auto;
              margin-top: -13px;
              border-radius: 4px;
              font-family: 'calibri';
              color: white;
              box-shadow: 30px 30px 500px #000; 
          }
            
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none; 
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
      
    
  <div class="container">
        <div class="row justify-content-center mt-4 pt-4">
            <div class="col-md-6">
        
        
        <form class="form-horizontal" id="formCategoria" name="frmCategoria" action="<%= request.getContextPath() %>/categorias.go" method="POST">
            <img class="rounded mx-auto d-block" src="assets/img/IconoCrearCategoria.png" alt="" width="100" height="100"><br>
            <h3 align="center">Mantenimiento Categorias</h3><br>
            <input type="hidden" name="id_categoria" value="<%= categoria.getId_categoria() %>" >
            <div class="form-group">
                <label for="txtNomCategoria" class="col-sm-2 control-label">Nombre:</label> 
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="txtNomCategoria" placeholder="Ingrese la nueva categoria" value="<%= categoria.getNom_categoria() %>" >
                </div>
            </div>
            <div class="form-group">
                <label for="txtEstadoCategoria" class="col-sm-2 control-label" >Estado:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="txtEstadoCategoria" value="<%= categoria.getEstado_categoria() %>" >
                </div>
            </div>
                <br>
            <div class="form-group" align='center'>
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" class="btn btn-success btn-lg" name="btnGuardar" value="Guardar" />
                    <input type="button" class="btn btn-danger btn-lg" onclick="regresar('<%= request.getContextPath() %>/categorias.go?opcion=listar')"
                           name="btnRegresar" value="Regresar" />
                </div>
                           <br>    
        <!-- Vista parcial de la parte inferior de nuestra aplicación -->
        <%@include file="../WEB-INF/vistas-parciales/pie.jspf" %>
        </div>
        </form>
    </body>
</html>
<%
            }
               
            }catch(Exception e){

            }
   %>
