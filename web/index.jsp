<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="Usuario1" scope="session" class="model.Usuario" />
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Inicio de sesión</title>
    
   
     <%@include file = "WEB-INF/vistas-parciales/css-js.jspf" %>
     <link href="bootstrap/css/signin.css" type="text/css" rel="stylesheet">
  </head>
  <body background="assets/img/fondo.jpg">
    <style>
        body{
            padding: 1%;
            margin-bottom: 1%;
            margin-top: 5%;
        }
          form{
              height: 710px;
              width: 600px;
              background: #000;
              padding: 30px;
              margin: auto;
              margin-top: 70px;
              margin-bottom: 50px;
              border-radius: 5px;
              font-family: 'calibri';
              color: white;
              box-shadow: 30px 30px 500px #000; 
          }
            
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none; 
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    
  
      <div class="container custom-container">
        <div class="col-sm-12 col-lg-12 col-xs-12 d-flex justify-content-center align-items-center">
  
      <form class="form-signin" action="loggin.go" method="POST">
          <input type="hidden" class="form-control" name="txtNomUsuario" value="<%= Usuario1.getNombre() %>" placeholder="Ingrese su nombre">
          <input type="hidden" class="form-control" name="txtApellidoUsuario" value="<%= Usuario1.getApellido() %>" placeholder="Ingrese su apellido">
          <input type="hidden" class="form-control" name="txtUsuUsuario" value="<%= Usuario1.getUsuario()%>" placeholder="Ingrese su usuario"  >
          <input type="hidden" class="form-control" name="txtEstadoUsuario" value="<%= Usuario1.getEstado() %>" placeholder="Ingrese 1 para Activo " >
          <input type="hidden" class="form-control" name="txtTipooUsuario" value="<%= Usuario1.getTipo() %>" placeholder="Ingrese su tipo " >
          <input type="hidden" class="form-control" name="txtPreguntaUsuario" value="<%= Usuario1.getPregunta() %>" >
          <input type="hidden" class="form-control" name="txtRespuestaUsuario" value="<%= Usuario1.getRespuesta() %>">
          <input type="hidden" name="opcion" value="login">
      <img class="rounded mx-auto d-block" src="assets/img/IconoInicioSesion.png" alt="" width="100" height="100"><br>
      <center><h1 class="h3 mb-3 font-weight-normal">Iniciar Sesión</h1></center>
      <label for="txtCorreoUsuario" class="form-label">Correo Electronico</label>
      <input type="email" name="txtCorreoUsuario" id="txtCorreoUsuario" class="form-control" placeholder="Dirección de correo electronico" required autofocus><br>
      <label for="txtClaveUsuarioa" class="form-label">Contraseña</label>
      <input type="password" name="txtClaveUsuario" id="txtClaveUsuario" class="form-control" placeholder="Contraseña" required><br>
      <div class="checkbox mb-3">
          <center>
        <label>
          <!--<input type="checkbox" value="remember-me"> Remember me -->
       
          <a href="loggin.go?opcion=registrar&&aviso=nuevo" title="¿No tienes cuenta?">¿No tienes cuenta? | </a>
          <a href="<%= request.getContextPath() %>/loggin.go?opcion=recuperarcontrasena" title="Olvide mi contraseña">Olvidé mi contraseña</a>
        </label>
        <label>
          <a href="<%= request.getContextPath() %>/loggin.go?opcion=recuperarcuenta" title="Olvide mi cuenta">Olvidé mi cuenta</a>
        </label>
      
          <center>
          <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
       <%@include file="WEB-INF/vistas-parciales/pie.jspf" %>
            </div>
          </div>
    </form>
            
            <%
                String mensaje = (String)request.getAttribute("Message");
                if(mensaje != "")
                {
            %>
            <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                  <img src="..." class="rounded me-2" alt="...">
                  <strong class="me-auto">Proyecto Final_Mensajepush</strong>
                  <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
                <div class="toast-body">
                  Cuenta creada con exito.
                </div>
              </div>
            <%
                }
            %>
</body>
</html>