# Proyecto Final - Inventario Web

# Integrantes                           Rol
- Francisco Jonatan Abarca roque        Scrum master / Developer back-end
- Neftali Eleazar Calderón Ruíz         Developer front-end
- Cristina Francisca Argueta Bichez     Developer front-end
- Nestór Áaron Orellana Lopez           Analista / Developer back-end
